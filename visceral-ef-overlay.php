<?php

/**
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           EF_Overlay
 *
 * @wordpress-plugin
 * Plugin Name:       Energy Foundation Overlay
 * Description:       Adds overlay to homepage
 * Version:           1.0.0
 * Author:            Visceral
 * Author URI:        https://thisisvisceral.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       visceral-ef-overlay
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'LOGIN_CUSTOMIZER_VERSION', '1.0.0' );
$plugin_url = plugin_dir_url( __FILE__ );

function visc_display_overlay()  { 
    if (is_front_page()) {   
        include("ef-overlay.html");
    }    
}

add_action( 'loop_end', 'visc_display_overlay' );

function visc_enqueue_style() {
    if (is_front_page()) {   
        wp_enqueue_style( 'visc-ef-overlay', plugins_url( '/styles/visc-ef-overlay.css', __FILE__ ), array(),'all' ); 
    }    
}

add_action( 'wp_enqueue_scripts', 'visc_enqueue_style', 99 );

function visc_add_overlay_fonts() {
    echo '<link rel="stylesheet" href="https://use.typekit.net/fme5tac.css">';
}

add_action( 'wp_head', 'visc_add_overlay_fonts' );
  




    

